# README #

- **author**  Jongho Yang (Joseph.Yang.MIITP@gmail.com)
- **date**    7/6/2017
- **version** 1.0
- **brief**	 [Android App] Shopping list

# DESCRIPTION #
  This is an android application to make shopping lists 
  with the sales information from New Zealand supermarkets for cost-effective shopping.
  Users easily can make their shopping lists and modify.

# Supported API version #
  Android API 21 and higher version

# Tested platform #
  Android Emulator and Samsung Galaxy S4

# Screenshot #
![CategoryView.png](https://bitbucket.org/repo/kMkLRXX/images/508429375-CategoryView.png)
![AddingAnItemToBuy.png](https://bitbucket.org/repo/kMkLRXX/images/3400428093-AddingAnItemToBuy.png)
![SupermaketList.png](https://bitbucket.org/repo/kMkLRXX/images/1186511946-SupermaketList.png)
![SaleInformationOfEachSupermarket.png](https://bitbucket.org/repo/kMkLRXX/images/2405642791-SaleInformationOfEachSupermarket.png)