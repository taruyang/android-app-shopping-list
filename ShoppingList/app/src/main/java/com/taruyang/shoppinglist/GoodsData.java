package com.taruyang.shoppinglist;

/**
 * Created by taruyang on 23/05/17.
 */

import java.util.ArrayList;

public class GoodsData {
    public static final String TAG = MainActivity.TAG;

    public static String[] goodsNameArray = {"Bakery", "Fruits", "Vegetables", "Meat", "Seafood", "Snacks", "Drinks", "Kitchenware", "Liquor", "Pet"};

    public static ArrayList<Goods> goodsList() {
        ArrayList<Goods> list = new ArrayList<>();

        for (int i = 0; i < goodsNameArray.length; i++) {
            Goods goods = new Goods();
            goods.name = goodsNameArray[i];
            goods.imageName = goodsNameArray[i].replaceAll("\\s+", "").toLowerCase();
            if (i == 2 || i == 5) {
                goods.isFav = true;
            }
            list.add(goods);
        }

        return (list);
    }
}
