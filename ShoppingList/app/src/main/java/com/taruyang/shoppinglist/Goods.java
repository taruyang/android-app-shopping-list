package com.taruyang.shoppinglist;

/**
 * Created by taruyang on 23/05/17.
 */
import android.content.Context;

public class Goods {

    public String   name;
    public String   imageName;
    public boolean  isFav;

    public int getImageResourceId(Context context) {
        int resourceID = context.getResources().getIdentifier(this.imageName, "drawable", context.getPackageName());
        return resourceID;
    }
}

