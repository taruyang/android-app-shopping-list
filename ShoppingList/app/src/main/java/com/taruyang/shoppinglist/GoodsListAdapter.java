package com.taruyang.shoppinglist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by taruyang on 23/05/17.
 */
public class GoodsListAdapter extends RecyclerView.Adapter<GoodsListAdapter.ViewHolder>{
    public static final String TAG = MainActivity.TAG;

    Context mContext;
    OnGoodsSelectedListener mItemClickListener;

    public GoodsListAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getItemCount() {
        return new GoodsData().goodsList().size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_goods, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Goods goods = new GoodsData().goodsList().get(position);
        holder.goodsName.setText(goods.name);
        Picasso.with(mContext).load(goods.getImageResourceId(mContext)).into(holder.goodsImage);

        Bitmap photo = BitmapFactory.decodeResource(mContext.getResources(), goods.getImageResourceId(mContext));

        Palette.generateAsync(photo, new Palette.PaletteAsyncListener() {
            public void onGenerated(Palette palette) {
                int bgColor = palette.getVibrantColor(mContext.getResources().getColor(android.R.color.black));
                holder.goodsNameHolder.setBackgroundColor(bgColor);
                holder.goodsNameHolder.setAlpha(0.7f);
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public LinearLayout goodsHolder;
        public LinearLayout goodsNameHolder;
        public TextView     goodsName;
        public ImageView    goodsImage;

        public ViewHolder(View itemView) {
            super(itemView);
            goodsHolder = (LinearLayout) itemView.findViewById(R.id.mainHolder);
            goodsName = (TextView) itemView.findViewById(R.id.goodsName);
            goodsNameHolder = (LinearLayout) itemView.findViewById(R.id.goodsNameHolder);
            goodsImage = (ImageView) itemView.findViewById(R.id.goodsImage);
            goodsHolder.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(mItemClickListener != null) {
                mItemClickListener.onItemClick(view, getPosition());
            }
        }
    }

    public interface OnGoodsSelectedListener{
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnGoodsSelectedListener itemClickListener) {
        this.mItemClickListener = itemClickListener;
    }
}
