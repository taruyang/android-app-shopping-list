package com.taruyang.shoppinglist;

/**
 * Created by taruyang on 23/05/17.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends Activity {
    public static final String TAG = "ShoppingList";

    private Menu menu;
    private boolean isListView;

    private RecyclerView mList;
    private StaggeredGridLayoutManager mLayoutManager;
    private GoodsListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mList = (RecyclerView)findViewById(R.id.list);
        mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mList.setLayoutManager(mLayoutManager);
        isListView = true;

        mAdapter = new GoodsListAdapter(this);
        mList.setAdapter(mAdapter);

        GoodsListAdapter.OnGoodsSelectedListener onItemClickListener = new GoodsListAdapter.OnGoodsSelectedListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(MainActivity.this, MemoActivity.class);
                intent.putExtra(MemoActivity.EXTRA_PARAM_ID, position);
                startActivity(intent);
            }
        };

        mAdapter.setOnItemClickListener(onItemClickListener);
    }

    @Override
    protected void onResume() {

        super.onResume();
        // With current senario, Nothing to do for resume
    }

    @Override
    protected void onPause() {

        super.onPause();
        // With current senario, Nothing to do for pause
    }

    @Override
    protected void onStop() {

        super.onStop();
        // With current senario, Nothing to do for stop
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_toggle) {
            toggle();
            return true;
        } else if(id == R.id.action_sale) {
            Intent intent = new Intent(MainActivity.this, SalesActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggle() {
        MenuItem item = menu.findItem(R.id.action_toggle);
        if (isListView) {
            mLayoutManager.setSpanCount(2);
            item.setIcon(R.drawable.ic_action_list);
            item.setTitle("Show as list");
            isListView = false;
        } else {
            mLayoutManager.setSpanCount(1);
            item.setIcon(R.drawable.ic_action_grid);
            item.setTitle("Show as grid");
            isListView = true;
        }
    }
}
