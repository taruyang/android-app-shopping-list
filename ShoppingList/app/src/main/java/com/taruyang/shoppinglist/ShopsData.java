package com.taruyang.shoppinglist;

import java.util.ArrayList;

/**
 * Created by taruyang on 28/05/17.
 */

public class ShopsData {

    public static String[] shopsNameArray = {"Countdown", "Four Square", "FreshChoice", "New World", "Pakn Save", "Price Chopper", "SuperValue", "Woolworths"};
    public static String[] shopsURL = {
            "https://www.countdown.co.nz/helping-you-save/weekly-mailer",
            "http://www.foursquare.co.nz/promotions-competitions/",
            "http://freshchoice.co.nz/savings",
            "http://www.newworld.co.nz/savings/?gclid=CNDjwoucktQCFdAHKgodKXMOjg",
            "http://www.paknsave.co.nz/promotions-and-deals/",
            "http://www.pricechopper.com/stores?from=WeeklyFlyer",
            "http://supervalue.co.nz/savings",
            "https://www.woolworths.com.au/Shop/SpecialsGroups/half-price"
    };

    public static ArrayList<Shop> shopsList() {
        ArrayList<Shop> list = new ArrayList<>();

        for (int i = 0; i < shopsNameArray.length; i++) {
            Shop shop = new Shop();
            shop.name = shopsNameArray[i];
            shop.url = shopsURL[i];
            shop.imageName = shopsNameArray[i].replaceAll("\\s+", "").toLowerCase();
            if (i == 2 || i == 5) {
                shop.isFav = true;
            }
            list.add(shop);
        }

        return (list);
    }
}
