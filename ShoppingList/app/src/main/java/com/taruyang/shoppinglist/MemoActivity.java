package com.taruyang.shoppinglist;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.taruyang.shoppinglist.R.id.editTextHolder;

/**
 * Created by taruyang on 23/05/17.
 */
public class MemoActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {
    public static final String TAG = MainActivity.TAG;
    public static final String EXTRA_PARAM_ID = "goods_id";
    private final String PREFS_TASKS = "prefs_tasks";

    // To get Time information
    private BroadcastReceiver mTickReceiver;
    private String mCurTime;
    // For views on activity_detail.xml
    private ListView mItems;
    private ImageView mImageView;
    private TextView mCategory;
    private LinearLayout mCategoryHolder;
    private ImageButton mAddButton;
    private LinearLayout mEditTextHolder;
    private EditText mEditTextToBuy;

    private boolean isEditing;
    private InputMethodManager mInputManager;
    private Goods mGoods;
    private ArrayList<String> mToBuyList;
    private ArrayAdapter mToBuyAdapter;
    int defaultColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail);

        mGoods = GoodsData.goodsList().get(getIntent().getIntExtra(EXTRA_PARAM_ID, 0));

        mItems = (ListView) findViewById(R.id.list_item);
        mImageView = (ImageView) findViewById(R.id.goodsImage);
        mCategory = (TextView) findViewById(R.id.category);
        mCategoryHolder = (LinearLayout) findViewById(R.id.categoryHolder);
        mAddButton = (ImageButton) findViewById(R.id.btn_add);
        mEditTextHolder = (LinearLayout) findViewById(editTextHolder);
        mEditTextToBuy = (EditText) findViewById(R.id.toBuy);

        mAddButton.setOnClickListener(this);
        mItems.setOnItemClickListener(this);

        defaultColor = getResources().getColor(R.color.primary_dark);
        mEditTextHolder.setVisibility(View.INVISIBLE);

        mInputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        isEditing = false;

        mCurTime = getCurrentTime();

        mTickReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Intent.ACTION_TIME_TICK)) {
                    mCurTime = getCurrentTime();
                }
            }
        };

        setUpAdapter();
        loadGoods();
        ///loadStoredData();
        applyAdaptiveColors();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadStoredData();
        registerReceiver(mTickReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    @Override
    protected void onPause() {
        super.onPause();
        storeData();
        if (mTickReceiver != null) {
            try {
                unregisterReceiver(mTickReceiver);
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "IllegalArgumentException !!! ", e);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void setUpAdapter() {
        mToBuyList = new ArrayList<>();
        mToBuyAdapter = new ArrayAdapter(this, R.layout.row_todo, mToBuyList);
        mItems.setAdapter(mToBuyAdapter);
    }

    private void loadStoredData() {
        String savedList = getSharedPreferences(PREFS_TASKS, MODE_PRIVATE).getString(mGoods.name, null);
        if (savedList != null) {
            String[] items = savedList.split(",");
            for (String s : items) {
                mToBuyList.add(s);
            }
        }
    }

    private void storeData() {
        // Save all data which you want to persist.
        StringBuilder savedList = new StringBuilder();
        for (String s : mToBuyList) {
            savedList.append(s);
            savedList.append(",");
        }
        getSharedPreferences(PREFS_TASKS, MODE_PRIVATE).edit()
                .putString(mGoods.name, savedList.toString()).commit();
    }

    private void loadGoods() {
        mCategory.setText(mGoods.name);
        mImageView.setImageResource(mGoods.getImageResourceId(this));
    }

    private void applyAdaptiveColors() {
        Bitmap photo = BitmapFactory.decodeResource(getResources(), mGoods.getImageResourceId(this));
        Palette mPalette = Palette.generate(photo);
        getWindow().setBackgroundDrawable(new ColorDrawable(mPalette.getDarkMutedColor(defaultColor)));
        mCategoryHolder.setBackgroundColor(mPalette.getMutedColor(defaultColor));
        mCategoryHolder.setAlpha(0.7f);
        mEditTextHolder.setBackgroundColor(mPalette.getLightVibrantColor(defaultColor));
        mEditTextHolder.setAlpha(0.7f);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add:
                if (!isEditing) {
                    mEditTextHolder.setVisibility(View.VISIBLE);
                    mEditTextToBuy.setText("");
                    mEditTextToBuy.requestFocus();
                    mInputManager.showSoftInput(mEditTextToBuy, InputMethodManager.SHOW_IMPLICIT);
                    isEditing = true;
                    mAddButton.setImageResource(R.drawable.icn_done);
                } else {
                    mEditTextHolder.setVisibility(View.INVISIBLE);
                    mToBuyList.add("[" + mCurTime + "] " + mEditTextToBuy.getText().toString());
                    mToBuyAdapter.notifyDataSetChanged();
                    mInputManager.hideSoftInputFromWindow(mEditTextToBuy.getWindowToken(), 0);
                    isEditing = false;
                    mAddButton.setImageResource(R.drawable.icn_add);
                }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        itemSelected(i);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private static String getCurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM");
        Date now = new Date();
        String strDate = dateFormat.format(now);
        return strDate;
    }

    private void itemSelected(final int position) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MemoActivity.this);

        alertDialogBuilder.setTitle(R.string.alert_title);

        alertDialogBuilder
                .setMessage(mToBuyList.get(position))
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mToBuyList.remove(position);
                        mToBuyAdapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }
}
