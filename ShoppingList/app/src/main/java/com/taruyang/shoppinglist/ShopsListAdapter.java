package com.taruyang.shoppinglist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by taruyang on 28/05/17.
 */

public class ShopsListAdapter extends RecyclerView.Adapter<ShopsListAdapter.ViewHolder> {
    public static final String TAG = MainActivity.TAG;

    Context mContext;
    OnShopSelectedListener mItemClickListener;

    public ShopsListAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getItemCount() {
        return new ShopsData().shopsList().size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_goods, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Shop shop = new ShopsData().shopsList().get(position);
        holder.shopName.setText(shop.name);
        Picasso.with(mContext).load(shop.getImageResourceId(mContext)).into(holder.shopImage);

        Bitmap photo = BitmapFactory.decodeResource(mContext.getResources(), shop.getImageResourceId(mContext));

        Palette.generateAsync(photo, new Palette.PaletteAsyncListener() {
            public void onGenerated(Palette palette) {
                int bgColor = palette.getVibrantColor(mContext.getResources().getColor(android.R.color.black));
                holder.shopNameHolder.setBackgroundColor(bgColor);
                holder.shopNameHolder.setAlpha(0.7f);
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public LinearLayout shopHolder;
        public LinearLayout shopNameHolder;
        public TextView     shopName;
        public ImageView    shopImage;

        public ViewHolder(View itemView) {
            super(itemView);
            shopHolder = (LinearLayout) itemView.findViewById(R.id.mainHolder);
            shopName = (TextView) itemView.findViewById(R.id.goodsName);
            shopNameHolder = (LinearLayout) itemView.findViewById(R.id.goodsNameHolder);
            shopImage = (ImageView) itemView.findViewById(R.id.goodsImage);
            shopHolder.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(mItemClickListener != null) {
                mItemClickListener.onItemClick(view, getPosition());
            }
        }
    }

    public interface OnShopSelectedListener{
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnShopSelectedListener itemClickListener) {
        this.mItemClickListener = itemClickListener;
    }
}
