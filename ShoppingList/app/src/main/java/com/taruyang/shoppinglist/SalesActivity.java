package com.taruyang.shoppinglist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

/**
 * Created by taruyang on 28/05/17.
 */

public class SalesActivity  extends Activity {
    public static final String TAG = MainActivity.TAG;

    private Menu menu;
    private boolean isListView;

    private RecyclerView mList;
    private StaggeredGridLayoutManager mLayoutManager;
    private ShopsListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales);

        mList = (RecyclerView)findViewById(R.id.list);
        mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mList.setLayoutManager(mLayoutManager);
        isListView = true;

        mAdapter = new ShopsListAdapter(this);
        mList.setAdapter(mAdapter);

        ShopsListAdapter.OnShopSelectedListener onItemClickListener = new ShopsListAdapter.OnShopSelectedListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(SalesActivity.this, WebActivity.class);
                intent.putExtra(WebActivity.EXTRA_PARAM_ID, position);
                startActivity(intent);

            }
        };

        mAdapter.setOnItemClickListener(onItemClickListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sales, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_toggle) {
            toggle();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggle() {
        MenuItem item = menu.findItem(R.id.action_toggle);
        if (isListView) {
            mLayoutManager.setSpanCount(2);
            item.setIcon(R.drawable.ic_action_list);
            item.setTitle("Show as list");
            isListView = false;
        } else {
            mLayoutManager.setSpanCount(1);
            item.setIcon(R.drawable.ic_action_grid);
            item.setTitle("Show as grid");
            isListView = true;
        }
    }
}
