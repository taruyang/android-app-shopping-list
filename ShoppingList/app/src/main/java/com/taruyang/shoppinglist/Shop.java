package com.taruyang.shoppinglist;

import android.content.Context;

public class Shop {

    public String   name;
    public String   imageName;
    public String   url;
    public boolean  isFav;

    public int getImageResourceId(Context context) {
        int resourceID = context.getResources().getIdentifier(this.imageName, "drawable", context.getPackageName());
        return resourceID;
    }
}
