package com.taruyang.shoppinglist;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by taruyang on 28/05/17.
 */

public class WebActivity extends Activity {
    public static final String TAG = MainActivity.TAG;

    public static final String EXTRA_PARAM_ID = "shop_url";
    private Shop    mShop;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        mShop = ShopsData.shopsList().get(getIntent().getIntExtra(EXTRA_PARAM_ID, 0));

        WebView saving = (WebView) findViewById(R.id.web);
        saving.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
        saving.loadUrl(mShop.url);
    }
}
